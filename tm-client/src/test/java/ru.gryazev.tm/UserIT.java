package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.gryazev.tm.endpoint.*;

import java.lang.Exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserIT {

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private static String token;

    @Nullable
    private static String tokenAdmin;

    @Nullable
    private static String userId;

    @Nullable
    private static String adminId;

    @BeforeClass
    public static void createTestUsers() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test");
        user.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        user.setRoleType(RoleType.USER);
        user.setName("Vasya");
        userId = userEndpoint.createUser(user).getId();
        token = sessionEndpoint.createSession(user.getLogin(), user.getPwdHash());

        @NotNull final User admin = new User();
        admin.setLogin("testadmin");
        admin.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        admin.setRoleType(RoleType.ADMIN);
        admin.setName("VasyaAdmin");
        adminId = userEndpoint.createUser(admin).getId();
        tokenAdmin = sessionEndpoint.createSession(admin.getLogin(), admin.getPwdHash());
    }

    @Test(expected = Exception.class)
    public void userFindByIdFail() throws Exception {
        @Nullable final User user = userEndpoint.findOneUser(token, adminId);
    }

    @Test
    public void userFindByIdOk() throws Exception {
        @Nullable final User user = userEndpoint.findOneUser(tokenAdmin, userId);
        assertNotNull(user);
    }

    @Test
    public void editUserOk() throws Exception {
        @Nullable final User user = userEndpoint.findCurrentUser(token);
        if (user == null) throw new Exception();
        user.setName("editedName");
        user.setLogin("editedLogin");
        userEndpoint.editUser(token, user);
        assertEquals("editedName", userEndpoint.findOneUser(tokenAdmin, userId).getName());
        assertEquals("editedLogin", userEndpoint.findOneUser(tokenAdmin, userId).getLogin());
    }

    @Test
    public void editUserFail() throws Exception {
        @Nullable final User user = userEndpoint.findCurrentUser(token);
        if (user == null) throw new Exception();
        user.setId(adminId);
        user.setName("editedName");
        user.setLogin("editedLogin");
        userEndpoint.editUser(token, user);
        assertEquals("VasyaAdmin", userEndpoint.findOneUser(tokenAdmin, adminId).getName());
    }

    @Test
    public void userRemoveSelf() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("toRemove");
        user.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        user.setRoleType(RoleType.USER);
        user.setName("VasyaToBeRemoved");
        userEndpoint.createUser(user);
        @NotNull final String tokenToRemove = sessionEndpoint.createSession(user.getLogin(), user.getPwdHash());
        final int count = userEndpoint.findAllUser(tokenAdmin).size();
        userEndpoint.removeUserSelf(tokenToRemove);
        assertEquals(count - 1, userEndpoint.findAllUser(tokenAdmin).size());
    }

    @Test(expected = Exception.class)
    public void findAllUserNotAdmin() throws Exception {
        userEndpoint.findAllUser(token);
    }

    @Test
    public void findAllUserAdmin() throws Exception {
        assertEquals(2, userEndpoint.findAllUser(tokenAdmin).size());
    }

    @AfterClass
    public static void clean() throws Exception {
        userEndpoint.removeUserSelf(token);
        userEndpoint.removeUserSelf(tokenAdmin);
    }

}

package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.gryazev.tm.endpoint.*;

import java.io.IOException;
import java.lang.Exception;
import java.util.Date;

import static org.junit.Assert.*;

public class TaskIT {

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private static final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @Nullable
    private static String token;

    @Nullable
    private static String tokenAdmin;

    @Nullable
    private static String project1Id;

    @Nullable
    private static String project2Id;

    @Nullable
    private static String project3Id;

    @Nullable
    private static String userId;

    @Nullable
    private static String adminId;


    @BeforeClass
    public static void createTestUsersAndProjects() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test");
        user.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        user.setRoleType(RoleType.USER);
        user.setName("Vasya");
        userId = userEndpoint.createUser(user).getId();
        token = sessionEndpoint.createSession(user.getLogin(), user.getPwdHash());

        @NotNull final User admin = new User();
        admin.setLogin("testadmin");
        admin.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        admin.setRoleType(RoleType.ADMIN);
        admin.setName("VasyaAdmin");
        adminId = userEndpoint.createUser(admin).getId();
        tokenAdmin = sessionEndpoint.createSession(admin.getLogin(), admin.getPwdHash());

        @NotNull final Project project1 = ProjectIT.getProject(userId);
        project1.setName("project1");
        @NotNull final Project project2 = ProjectIT.getProject(userId);
        project2.setName("project2");
        @NotNull final Project project3 = ProjectIT.getProject(userId);
        project3.setName("project3");
        project1Id = projectEndpoint.createProject(token, project1).getId();
        project2Id = projectEndpoint.createProject(token, project2).getId();
        project3Id = projectEndpoint.createProject(token, project3).getId();
    }

    @Test
    public void taskCreateOk() throws Exception {
        final int count1 = taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size();
        final int count2 = taskEndpoint.findTaskByProjectSorted(token, project2Id, "default").size();

        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project2Id, userId));
        taskEndpoint.createTask(token, getTask(project2Id, userId));
        assertEquals(count1 + 1, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
        assertEquals(count2 + 2, taskEndpoint.findTaskByProjectSorted(token, project2Id, "default").size());
    }

    @Test(expected = Exception.class)
    public void taskCreateInvalidToken() throws Exception {
        taskEndpoint.createTask("InvalidToken", getTask(project1Id, userId));
    }

    @Test
    public void taskEditOk() throws Exception {
        @NotNull final Task createdTask = taskEndpoint.createTask(token, getTask(project1Id, userId));
        createdTask.setName("edited-task");
        taskEndpoint.editTask(token, createdTask);
        assertEquals("edited-task", taskEndpoint.findOneTask(token, createdTask.getId()).getName());
    }

    @Test
    public void taskEditAnotherUser() throws Exception {
        @NotNull final Task createdTask = taskEndpoint.createTask(token, getTask(project1Id, userId));
        createdTask.setName("edited-task");
        @Nullable final Task editedTask = taskEndpoint.editTask(tokenAdmin, createdTask);
        assertNull(editedTask);
    }

    @Test
    public void taskEditInvalidId() throws Exception{
        @NotNull final Task createdTask = taskEndpoint.createTask(token, getTask(project1Id, userId));
        @NotNull final String oldId = createdTask.getId();
        createdTask.setId("invalidId");
        taskEndpoint.editTask(token, createdTask);
        assertNotNull(taskEndpoint.findOneTask(token, oldId));
        assertNotNull(taskEndpoint.findOneTask(token, "invalidId"));
    }

    @Test
    public void taskRemoveOk() throws Exception {
        final int count = taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size();
        @NotNull final Task createdTask = taskEndpoint.createTask(token, getTask(project1Id, userId));
        assertEquals(count + 1, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
        taskEndpoint.removeTask(token, createdTask.getId());
        assertEquals(count, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
    }

    @Test
    public void taskRemoveAnotherUser() throws Exception {
        final int count = taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size();
        @NotNull final Task createdTask = taskEndpoint.createTask(token, getTask(project1Id, userId));
        assertEquals(count + 1, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
        taskEndpoint.removeTask(tokenAdmin, createdTask.getId());
        assertEquals(count + 1, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
    }

    @Test(expected = Exception.class)
    public void taskRemoveAllUser() throws Exception {
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.removeAllTask(token);
    }

    @Test
    public void taskRemoveAllAdmin() throws Exception {
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.removeAllTask(tokenAdmin);
        assertEquals(0, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
    }

    @Test
    public void taskUnlinkAndList() throws Exception {
        final int countUnlinked = taskEndpoint.listTaskUnlinked(token).size();
        @NotNull final Task task1 = taskEndpoint.createTask(token, getTask(project1Id, userId));
        @NotNull final Task task2 = taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.unlinkTask(token, task1);
        assertEquals(countUnlinked + 1, taskEndpoint.listTaskUnlinked(token).size());
        taskEndpoint.unlinkTask(token, task2);
        assertEquals(countUnlinked + 2, taskEndpoint.listTaskUnlinked(token).size());
    }

    @Test
    public void taskLink() throws Exception {
        final int countTasksOfProj1 = taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size();
        final int countTasksOfProj2 = taskEndpoint.findTaskByProjectSorted(token, project2Id, "default").size();
        @NotNull final Task task1 = taskEndpoint.createTask(token, getTask(project1Id, userId));
        @NotNull final Task task2 = taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.linkTask(token, project1Id, project2Id, 0);
        assertEquals(countTasksOfProj1 + 1, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
        assertEquals(countTasksOfProj2 + 1, taskEndpoint.findTaskByProjectSorted(token, project2Id, "default").size());
    }

    @Test
    public void taskRemoveByProjectId() throws Exception {
        final int count = taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size();
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        taskEndpoint.createTask(token, getTask(project1Id, userId));
        assertEquals(count + 3, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
        taskEndpoint.removeByProjectId(token, project1Id);
        assertEquals(0, taskEndpoint.findTaskByProjectSorted(token, project1Id, "default").size());
    }

    @Test
    public void taskFindByName() throws Exception {
        taskEndpoint.removeAllTask(tokenAdmin);
        @NotNull final Task task1 = getTask(project1Id, userId);
        task1.setName("task");
        @NotNull final Task task2 = getTask(project1Id, userId);
        task2.setName("task");
        @NotNull final Task task3 = getTask(project1Id, userId);
        task3.setName("task");
        @NotNull final Task task4 = getTask(project1Id, userId);
        task4.setName("wowItsProject!");
        @NotNull final Task task5 = getTask(project1Id, userId);
        task5.setName("wowItsProject!");
        taskEndpoint.createTask(token, task1);
        taskEndpoint.createTask(token, task2);
        taskEndpoint.createTask(token, task3);
        taskEndpoint.createTask(token, task4);
        taskEndpoint.createTask(token, task5);
        assertEquals(3, taskEndpoint.findTaskByName(token, "tas").size());
        assertEquals(2, taskEndpoint.findTaskByName(token, "roje").size());
    }

    @AfterClass
    public static void clean() throws Exception {
        userEndpoint.removeUserSelf(token);
        userEndpoint.removeUserSelf(tokenAdmin);
    }

    @NotNull
    static Task getTask(@Nullable final String projectId, @Nullable final String userId) throws IOException {
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setName("task");
        task.setDetails("details-task");
        task.setDateStart(ProjectIT.toXmlGregorianDate(new Date()));
        task.setDateFinish(ProjectIT.toXmlGregorianDate(new Date()));
        task.setStatus(Status.PLANNED);
        return task;
    }

}

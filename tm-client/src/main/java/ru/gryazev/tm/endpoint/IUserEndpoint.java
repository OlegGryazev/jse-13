package ru.gryazev.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.3.6
 * 2020-05-20T09:32:21.518+03:00
 * Generated source version: 3.3.6
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.gryazev.ru/", name = "IUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IUserEndpoint {

    @WebMethod
    @RequestWrapper(localName = "findAllUser", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.FindAllUser")
    @ResponseWrapper(localName = "findAllUserResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.FindAllUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.gryazev.tm.endpoint.User> findAllUser(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    ) throws Exception_Exception;

    @WebMethod
    @RequestWrapper(localName = "editUser", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.EditUser")
    @ResponseWrapper(localName = "editUserResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.EditUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.gryazev.tm.endpoint.User editUser(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.gryazev.tm.endpoint.User arg1
    ) throws Exception_Exception;

    @WebMethod
    @RequestWrapper(localName = "removeUserSelf", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.RemoveUserSelf")
    @ResponseWrapper(localName = "removeUserSelfResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.RemoveUserSelfResponse")
    public void removeUserSelf(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    ) throws Exception_Exception;

    @WebMethod
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.CreateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.gryazev.tm.endpoint.User createUser(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.gryazev.tm.endpoint.User arg0
    ) throws Exception_Exception;

    @WebMethod
    @RequestWrapper(localName = "findOneUser", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.FindOneUser")
    @ResponseWrapper(localName = "findOneUserResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.FindOneUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.gryazev.tm.endpoint.User findOneUser(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    ) throws Exception_Exception;

    @WebMethod
    @RequestWrapper(localName = "findCurrentUser", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.FindCurrentUser")
    @ResponseWrapper(localName = "findCurrentUserResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.FindCurrentUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.gryazev.tm.endpoint.User findCurrentUser(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0
    ) throws Exception_Exception;

    @WebMethod
    @RequestWrapper(localName = "getUserId", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.GetUserId")
    @ResponseWrapper(localName = "getUserIdResponse", targetNamespace = "http://endpoint.tm.gryazev.ru/", className = "ru.gryazev.tm.endpoint.GetUserIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getUserId(

        @WebParam(name = "arg0", targetNamespace = "")
        java.lang.String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        int arg1
    ) throws Exception_Exception;
}

package ru.gryazev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Status;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public class ProjectSetStatus extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-set-status";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Set entered status to project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final IProjectEndpoint projectServiceEndpoint = serviceLocator.getProjectEndpoint();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectServiceEndpoint.getProjectId(token, projectIndex);
        if (projectId == null) throw new CrudNotFoundException();

        @Nullable final Project project = projectServiceEndpoint.findOneProject(token, projectId);
        if (project == null) throw new CrudNotFoundException();
        @Nullable final Status status = terminalService.getStatus();
        if (status == null) throw new CrudUpdateException();
        project.setStatus(status);
        @Nullable final Project editedProject = projectServiceEndpoint.editProject(token, project);
        if (editedProject == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}

package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskUnlinkCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-unlink";
    }

    @Override
    public String getDescription() {
        return "Unlink selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final String token = getToken();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final String currentProjectId = getCurrentProjectId();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskEndpoint.getTaskId(token, currentProjectId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @Nullable final Task task = taskEndpoint.findOneTask(token, taskId);
        @Nullable final Task unlinkedTask = taskEndpoint.unlinkTask(token, task);
        if (unlinkedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}

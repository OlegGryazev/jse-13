package ru.gryazev.tm.command.project.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.entity.Setting;

@NoArgsConstructor
public class ProjectSortDateFinish extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sort-date-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts projects in order of date of project finish.";
    }

    @Override
    public void execute() {
        if (terminalService == null || settingService == null) return;
        settingService.setSetting(new Setting("project-sort", "date-finish"));
        terminalService.print("[OK]");
    }

}

package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;

public class JsonSaveFasterxmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save-json-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save all data in JSON format using Fasterxml.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final String token = getToken();
        serviceLocator.getDomainEndpoint().saveJsonFasterxml(token);
        terminalService.print("[OK]");
    }

}

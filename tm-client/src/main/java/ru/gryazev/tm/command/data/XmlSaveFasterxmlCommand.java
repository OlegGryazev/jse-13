package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;

public class XmlSaveFasterxmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "save-xml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save all data in XML format using Fasterxml.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final String token = getToken();
        serviceLocator.getDomainEndpoint().saveXmlFasterxml(token);
        terminalService.print("[OK]");
    }

}

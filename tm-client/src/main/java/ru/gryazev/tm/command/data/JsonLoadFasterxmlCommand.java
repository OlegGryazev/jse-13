package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;

public class JsonLoadFasterxmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load-json-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loads data from JSON using Fasterxml.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final String token = getToken();
        serviceLocator.getDomainEndpoint().loadJsonFasterxml(token);
        terminalService.print("[OK]");
    }

}

package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;

public class XmlLoadJaxbCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load-xml-jaxb";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Loads data from XML using JAXB.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final String token = getToken();
        serviceLocator.getDomainEndpoint().loadXmlJaxb(token);
        terminalService.print("[OK]");
    }

}

package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskLinkCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-link";
    }

    @Override
    public String getDescription() {
        return "Link selected task to project.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final String token = getToken();
        @Nullable final String currentProjectId = getCurrentProjectId();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = serviceLocator.getProjectEndpoint()
                .getProjectId(token, projectIndex);
        final int taskIndex = terminalService.getTaskIndex();

        @Nullable final Task linkedTask = serviceLocator.getTaskEndpoint()
                .linkTask(token, currentProjectId, projectId, taskIndex);
        if (linkedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}

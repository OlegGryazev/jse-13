package ru.gryazev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.Exception_Exception;

public class SerLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "load";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from file using serialization.";
    }

    @Override
    public void execute() throws Exception_Exception {
        if (terminalService == null || serviceLocator == null) return;
        @Nullable final String token = getToken();
        serviceLocator.getDomainEndpoint().loadSer(token);
        terminalService.print("[OK]");
    }

}

package ru.gryazev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ISessionFactoryService sessionFactoryService = new SessionFactoryService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(sessionFactoryService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(sessionFactoryService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(sessionFactoryService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionFactoryService);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint();

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint();

    public void init() throws Exception {
        domainService.setServiceLocator(this);
        projectEndpoint.setServiceLocator(this);
        taskEndpoint.setServiceLocator(this);
        userEndpoint.setServiceLocator(this);
        sessionEndpoint.setServiceLocator(this);
        domainEndpoint.setServiceLocator(this);
        Endpoint.publish("http://localhost:8080/session?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/project?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/task?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/user?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/domain?wsdl", domainEndpoint);
    }

}
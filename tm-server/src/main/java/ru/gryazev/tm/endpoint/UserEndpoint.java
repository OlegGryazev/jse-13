package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Nullable
    public User findOneUser(@Nullable final String token, @Nullable final String userId) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        return serviceLocator.getUserService().findOne(session.getUserId(), userId);
    }

    @Nullable
    public User findCurrentUser(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().findOne(session.getUserId(), session.getUserId());
    }

    @Nullable
    @Override
    public User createUser(@Nullable User user) throws Exception {
        if (serviceLocator == null || user == null) return null;
        return serviceLocator.getUserService().create(user.getId(), user);
    }

    @Nullable
    @Override
    public User editUser(@Nullable final String token, @Nullable final User user) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().edit(session.getUserId(), user);
    }

    @NotNull
    @Override
    public List<User> findAllUser(@Nullable String token) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        return serviceLocator.getUserService().findAll();
    }

    @Override
    public void removeUserSelf(@Nullable String token) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getUserService().remove(session.getUserId(), session.getUserId());
    }

    @Nullable
    @Override
    public String getUserId(@Nullable String token, int userIndex) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getUserService().getUserId(userIndex);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

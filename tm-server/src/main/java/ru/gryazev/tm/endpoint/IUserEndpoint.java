package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.User;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable
    public User findOneUser(@Nullable String token, @Nullable String userId) throws Exception;

    @Nullable
    public User findCurrentUser(@Nullable String token) throws Exception;

    @Nullable
    public User editUser(@Nullable String token, @Nullable User user) throws Exception;

    @Nullable
    public User createUser(@Nullable User user) throws Exception;

    public void removeUserSelf(@Nullable String token) throws Exception;

    @Nullable
    public String getUserId(@Nullable String token, int userIndex) throws Exception;

    @NotNull
    public List<User> findAllUser(@Nullable String token) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}

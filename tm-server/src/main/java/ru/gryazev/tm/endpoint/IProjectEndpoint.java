package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Project;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    public List<Project> findAllProject(@Nullable String token) throws Exception;

    @Nullable
    public Project createProject(@Nullable String token, @Nullable Project project) throws Exception;

    @Nullable
    public Project findOneProject(@Nullable String token, @Nullable String entityId) throws Exception;

    @NotNull
    public List<Project> findProjectByUserId(@Nullable String token) throws Exception;

    @NotNull
    public List<Project> findProjectByUserIdSorted(@Nullable String token, @Nullable String sortType) throws Exception;

    @Nullable
    public Project editProject(@Nullable String token, @Nullable Project project) throws Exception;

    @Nullable
    public String getProjectId(@Nullable String token, int projectIndex) throws Exception;

    public void removeProject(@Nullable String token, @Nullable String entityId) throws Exception;

    public void removeAllProject(@Nullable String token) throws Exception;

    @NotNull
    public List<Project> findProjectByDetails(@NotNull String token, @NotNull String projectDetails) throws Exception;

    @NotNull
    public List<Project> findProjectByName(@Nullable String token, @Nullable String projectName) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}

package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.sql.SQLException;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Override
    public void removeSession(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getSessionService().remove(session.getUserId(), session.getId());
    }

    @Nullable
    @Override
    public String createSession(@Nullable final String login, @Nullable final String pwdHash) throws Exception {
        if (serviceLocator == null) return null;
        return serviceLocator.getSessionService().create(login, pwdHash);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;

import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    public void removeSession(@Nullable String token) throws Exception;

    @Nullable
    public String createSession(@Nullable String login, @Nullable String pwdHash) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}

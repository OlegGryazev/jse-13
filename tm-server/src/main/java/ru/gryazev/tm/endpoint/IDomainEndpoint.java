package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;

import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    public void saveJsonJaxb(@Nullable String token) throws Exception;

    public void saveXmlJaxb(@Nullable String token) throws Exception;

    public void saveJsonFasterxml(@Nullable String token) throws Exception;

    public void saveXmlFasterxml(@Nullable String token) throws Exception;

    public void saveSer(@Nullable String token) throws Exception;

    public void loadJsonJaxb(@Nullable String token) throws Exception;

    public void loadXmlJaxb(@Nullable String token) throws Exception;

    public void loadJsonFasterxml(@Nullable String token) throws Exception;

    public void loadXmlFasterxml(@Nullable String token) throws Exception;

    public void loadSer(@Nullable String token) throws Exception;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator);

}

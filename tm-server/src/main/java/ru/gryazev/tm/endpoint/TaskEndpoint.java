package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collections;
import java.util.List;

@WebService(endpointInterface = "ru.gryazev.tm.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Nullable
    private ServiceLocator serviceLocator;

    @Nullable
    @Override
    public Task findOneTask(@Nullable final String token, @Nullable final String taskId) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findOne(session.getUserId(), taskId);
    }

    @Nullable
    @Override
    public Task createTask(@Nullable final String token, @Nullable final Task task) throws Exception {
        if (serviceLocator == null || token == null || task == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().create(session.getUserId(), task);
    }

    @Override
    public void removeAllTask(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session, new RoleType[]{RoleType.ADMIN});
        serviceLocator.getTaskService().removeAll();
    }

    @Nullable
    @Override
    public Task editTask(@Nullable final String token, @Nullable final Task task) throws Exception {
        if (serviceLocator == null || token == null || task == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().edit(session.getUserId(), task);
    }

    @Override
    public void removeTask(@Nullable final String token, @Nullable final String taskId) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().remove(session.getUserId(), taskId);
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String token, @Nullable final String projectId, final int taskIndex) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().getTaskId(session.getUserId(), projectId, taskIndex);
    }

    @NotNull
    @Override
    public List<Task> findTaskByName(@Nullable final String token, @Nullable final String taskName) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findByName(session.getUserId(), taskName);
    }

    @NotNull
    @Override
    public List<Task> findTaskByDetails(@Nullable final String token, @Nullable final String taskDetails) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().findByDetails(session.getUserId(), taskDetails);
    }

    @Nullable
    @Override
    public Task linkTask(@Nullable final String token,
                         @Nullable final String oldProjectId,
                         @Nullable final String newProjectId,
                         final int taskIndex) throws Exception {
        if (serviceLocator == null || token == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().linkTask(session.getUserId(), oldProjectId, newProjectId, taskIndex);
    }

    @Override
    public @NotNull List<Task> findTaskByProjectSorted(@Nullable String token, @Nullable String projectId, @Nullable String sortType) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskByProjectSorted(session.getUserId(), projectId, sortType);
    }

    @Override
    public @NotNull List<Task> listTaskUnlinkedSorted(@Nullable String token, @Nullable String sortType) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskUnlinkedSorted(session.getUserId(), sortType);
    }

    @NotNull
    @Override
    public List<Task> listTaskByProject(@Nullable final String token, @Nullable final String projectId) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskByProject(session.getUserId(), projectId);
    }

    @NotNull
    @Override
    public  List<Task> listTaskUnlinked(@Nullable final String token) throws Exception {
        if (serviceLocator == null || token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().listTaskUnlinked(session.getUserId());
    }

    @Override
    public void removeByProjectId(@Nullable final String token, @Nullable final String projectId) throws Exception {
        if (serviceLocator == null || token == null) return;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        serviceLocator.getTaskService().removeByProjectId(session.getUserId(), projectId);
    }

    @Nullable
    @Override
    public Task unlinkTask(@Nullable final String token, @Nullable final Task task) throws Exception {
        if (serviceLocator == null || token == null || task == null) return null;
        @NotNull final Session session = getSessionFromToken(token, serviceLocator);
        serviceLocator.getSessionService().validateSession(session);
        return serviceLocator.getTaskService().unlinkTask(session.getUserId(), task);
    }

    @Override
    @WebMethod(exclude = true)
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}

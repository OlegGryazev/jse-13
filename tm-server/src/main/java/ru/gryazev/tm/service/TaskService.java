package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.mapper.TaskMapper;
import ru.gryazev.tm.api.service.ISessionFactoryService;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.Task;
import ru.gryazev.tm.util.CompareUtil;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class TaskService implements ITaskService {

    @NotNull
    final ISessionFactoryService sessionFactoryService;

    @Nullable
    @Override
    public Task create(@Nullable final String userId, @Nullable final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(task)) return null;
        if (!userId.equals(task.getUserId())) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try {
            mapper.persist(task);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task edit(@Nullable final String userId, @Nullable final Task task) throws Exception {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(task.getUserId())) return null;
        @Nullable final Task existingTask = findOne(userId, task.getId());
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try {
            if (existingTask != null) mapper.merge(task);
            else mapper.persist(task);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String taskId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @Nullable final Task task = mapper.findOneById(userId, taskId);
        sqlSession.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> listTaskUnlinked(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAllByUserIdUnlinked(userId);
        sqlSession.close();
        return tasks;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.remove(userId, taskId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try {
            mapper.removeByProjectId(userId, projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Task unlinkTask(@Nullable final String userId, @Nullable final Task task) throws Exception {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try {
            task.setProjectId(null);
            mapper.merge(task);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String userId, @Nullable final String projectId, final int taskIndex) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @Nullable final List<Task> tasks = mapper.findTasksByProjectId(userId, projectId);
        sqlSession.close();
        if (taskIndex >= tasks.size()) return null;
        return tasks.get(taskIndex).getId();
    }

    @NotNull
    @Override
    public List<Task> listTaskByProject(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findTasksByProjectId(userId, projectId);
        sqlSession.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task linkTask(@Nullable final String userId,
                         @Nullable final String oldProjectId,
                         @Nullable final String newProjectId,
                         final int taskIndex) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (newProjectId == null || newProjectId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @Nullable final List<Task> tasks = oldProjectId == null ?
                mapper.findAllByUserIdUnlinked(userId) :
                mapper.findTasksByProjectId(userId, oldProjectId);
        if (taskIndex >= tasks.size()) return null;
        @NotNull final Task task = tasks.get(taskIndex);
        task.setProjectId(newProjectId);
        try {
            mapper.merge(task);
            sqlSession.commit();
            return task;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findByName(@Nullable final String userId, @Nullable final String taskName) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAllByName(userId, taskName);
        sqlSession.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByDetails(@Nullable final String userId, @Nullable final String taskDetails) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskDetails == null || taskDetails.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAllByDetails(userId, taskDetails);
        sqlSession.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    ) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAllByProjectIdSorted(userId, projectId, sqlSortType);
        sqlSession.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAllByUserIdUnlinkedSorted(userId, sqlSortType);
        sqlSession.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAllByUserId(userId);
        sqlSession.close();
        return tasks;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final TaskMapper mapper = sqlSession.getMapper(TaskMapper.class);
        @NotNull final List<Task> tasks = mapper.findAll();
        sqlSession.close();
        return tasks;
    }

    public boolean isEntityValid(@Nullable final Task task) {
        if (task == null) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (task.getUserId() == null || task.getUserId().isEmpty()) return false;
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) return false;
        return (task.getName() != null && !task.getName().isEmpty());
    }

}
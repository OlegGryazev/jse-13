package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.mapper.ProjectMapper;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.api.service.ISessionFactoryService;
import ru.gryazev.tm.entity.Project;
import ru.gryazev.tm.util.CompareUtil;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class ProjectService implements IProjectService {

    @NotNull
    final ISessionFactoryService sessionFactoryService;

    @Nullable
    public String getProjectId(@Nullable final String userId, final int projectIndex) throws Exception {
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @Nullable final List<Project> projects = mapper.findAllByUserId(userId);
        sqlSession.close();
        return projectIndex >= projects.size() ? null : projects.get(projectIndex).getId();
    }

    @Nullable
    @Override
    public Project create(@Nullable final String userId, @Nullable final Project project) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(project)) return null;
        if (!userId.equals(project.getUserId())) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try {
            mapper.persist(project);
            sqlSession.commit();
            return project;
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project edit(@Nullable final String userId, @Nullable final Project project) throws Exception {
        if (!isEntityValid(project)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(project.getUserId())) return null;
        @Nullable final Project existingProject = findOne(userId, project.getId());
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try {
            if (existingProject != null) mapper.merge(project);
            else mapper.persist(project);
            sqlSession.commit();
            return project;
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @Nullable final Project project = mapper.findOneById(userId, projectId);
        sqlSession.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String userId, @Nullable final String projectName) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projects = mapper.findAllByName(userId, projectName);
        sqlSession.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDetails(@Nullable final String userId, @Nullable final String projectDetails) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectDetails == null || projectDetails.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projects = mapper.findAllByDetails(userId, projectDetails);
        sqlSession.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> list = mapper.findAllByUserId(userId);
        sqlSession.close();
        return list;
    }

    @NotNull
    @Override
    public List<Project> findByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> list = mapper.findAllSorted(userId, sqlSortType);
        sqlSession.close();
        return list;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.remove(userId, projectId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.removeAllByUserId(userId);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final ProjectMapper mapper = sqlSession.getMapper(ProjectMapper.class);
        @NotNull final List<Project> projects = mapper.findAll();
        sqlSession.close();
        return projects;
    }

    private boolean isEntityValid(@Nullable final Project project) {
        if (project == null) return false;
        if (project.getId() == null || project.getId().isEmpty()) return false;
        if (project.getName() == null || project.getName().isEmpty()) return false;
        return project.getUserId() != null && !project.getUserId().isEmpty();
    }

}

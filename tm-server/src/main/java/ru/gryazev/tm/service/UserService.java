package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.mapper.UserMapper;
import ru.gryazev.tm.api.service.ISessionFactoryService;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public final class UserService implements IUserService {

    @NotNull
    final ISessionFactoryService sessionFactoryService;

    @Nullable
    @Override
    public User create(@Nullable final String userId, @Nullable final User user) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(user)) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.persist(user);
            sqlSession.commit();
            return user;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }
    
    public boolean checkRole(@Nullable final String userId, @Nullable final RoleType[] roles) throws Exception {
        if (roles == null) return false;
        if (userId == null || userId.isEmpty()) return false;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @Nullable final User user = mapper.findOneById(userId);
        sqlSession.close();
        if (user == null) return false;
        return Arrays.asList(roles).contains(user.getRoleType());
    }

    @Nullable
    @Override
    public String getUserId(int userIndex) throws Exception {
        if (userIndex < 0) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @NotNull final List<User> users = mapper.findAll();
        sqlSession.close();
        return userIndex >= users.size() ? null : users.get(userIndex).getId();
    }

    public boolean isEntityValid(@Nullable final User user) {
        if (user == null || user.getRoleType() == null) return false;
        if (user.getId() == null || user.getId().isEmpty()) return false;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return false;
        return user.getPwdHash() != null && !user.getPwdHash().isEmpty();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @Nullable final User user = mapper.findOneById(id);
        sqlSession.close();
        return user;
    }

    @Nullable
    @Override
    public User edit(@Nullable final String userId, @Nullable final User user) throws Exception {
        if (!isEntityValid(user)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(user.getUserId())) return null;
        @Nullable final User existingUser = findOne(userId, user.getId());
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            if (existingUser != null) mapper.merge(user);
            else mapper.persist(user);
            sqlSession.commit();
            return user;
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.remove(id);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        @NotNull final List<User> users = mapper.findAll();
        sqlSession.close();
        return users;
    }

}

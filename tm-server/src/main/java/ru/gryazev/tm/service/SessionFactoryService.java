package ru.gryazev.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.mapper.ProjectMapper;
import ru.gryazev.tm.api.mapper.SessionMapper;
import ru.gryazev.tm.api.mapper.TaskMapper;
import ru.gryazev.tm.api.mapper.UserMapper;
import ru.gryazev.tm.api.service.IPropertyService;
import ru.gryazev.tm.api.service.ISessionFactoryService;

import javax.sql.DataSource;

public class SessionFactoryService implements ISessionFactoryService {

    @NotNull
    public SqlSessionFactory getSqlSessionFactory() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final String user = propertyService.getProperty("username");
        @NotNull final String password = propertyService.getProperty("password");
        @NotNull final String url = propertyService.getProperty("url");
        @Nullable final String driver = propertyService.getProperty("driver");
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.addMapper(UserMapper.class);
        configuration.addMapper(ProjectMapper.class);
        configuration.addMapper(TaskMapper.class);
        configuration.addMapper(SessionMapper.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}

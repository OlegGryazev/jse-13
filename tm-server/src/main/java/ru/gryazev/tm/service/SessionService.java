package ru.gryazev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.mapper.SessionMapper;
import ru.gryazev.tm.api.mapper.UserMapper;
import ru.gryazev.tm.api.service.ISessionFactoryService;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;
import ru.gryazev.tm.util.CryptUtil;
import ru.gryazev.tm.util.SignatureUtil;

import java.util.Arrays;
import java.util.Date;

@AllArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    final ISessionFactoryService sessionFactoryService;
    
    @Nullable
    @Override
    public String create(@Nullable final String login, @Nullable final String pwdHash) throws Exception {
        if (login == null || login.isEmpty()) return null;
        if (pwdHash == null || pwdHash.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        @Nullable final User user = userMapper.findByLoginAndPwd(login, pwdHash);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setRole(user.getRoleType());
        session.setUserId(user.getId());
        session.setSignature(SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT));
        @NotNull final SessionMapper sessionMapper = sqlSession.getMapper(SessionMapper.class);
        try {
            sessionMapper.persist(session);
            sqlSession.commit();
            return encryptSession(session);
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    private Session findOne(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        @Nullable final Session session = mapper.findOneById(userId, id);
        sqlSession.close();
        return session;
    }

    public void validateSession(@Nullable final Session session, @Nullable final RoleType[] roles) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null) throw new Exception(message);
        if (roles == null || !Arrays.asList(roles).contains(session.getRole())) throw new Exception(message);
        validateSession(session);
    }

    public void validateSession(@Nullable final Session session) throws Exception {
        @NotNull final String message = "Session is not valid!";
        if (session == null || session.getUserId() == null || session.getUserId().isEmpty()) throw new Exception(message);
        @NotNull final long sessionAliveTime = new Date().getTime() - session.getTimestamp();
        if (sessionAliveTime > Constant.SESSION_LIFETIME) throw new Exception(message);
        if (session.getRole() == null) throw new Exception(message);
        @Nullable final Session sessionAtServer = findOne(session.getUserId(), session.getId());
        if (sessionAtServer == null || sessionAtServer.getSignature() == null) throw new Exception(message);
        @Nullable final String actualSessionSignature =  SignatureUtil.sign(session, Constant.SALT, Constant.CYCLE_COUNT);
        if (!sessionAtServer.getSignature().equals(actualSessionSignature)) throw new Exception(message);
    }

    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        try{
            mapper.removeAll();
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession sqlSession = sessionFactoryService.getSqlSessionFactory().openSession();
        @NotNull final SessionMapper mapper = sqlSession.getMapper(SessionMapper.class);
        try{
            mapper.remove(userId, id);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw new Exception(e);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    private String encryptSession(@Nullable final Session session) throws Exception {
        if (session == null) return null;
        @NotNull final String key = new PropertyService().getProperty("key");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return CryptUtil.encrypt(json, key);
    }

}

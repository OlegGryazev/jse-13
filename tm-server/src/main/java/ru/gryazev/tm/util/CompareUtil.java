package ru.gryazev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CompareUtil {

    @NotNull
    public static String getSqlSortType(@Nullable final String sortType) {
        if (sortType == null) return "createMillis";
        switch (sortType) {
            case "default": return "createMillis";
            case "date-start": return "dateStart";
            case "date-finish": return "dateFinish";
            case "status": return "status";
        }
        return "timestamp";
    }

}

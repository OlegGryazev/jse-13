package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.entity.ComparableEntity;
import ru.gryazev.tm.entity.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    public List<Task> listTaskUnlinked(@Nullable String userId) throws Exception;

    public void removeByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable
    public Task unlinkTask(@Nullable String userId, @Nullable Task task) throws Exception;

    @Nullable
    public String getTaskId(@Nullable String userId,
                            @Nullable String projectId,
                            int taskIndex) throws Exception;

    @NotNull
    public List<Task> listTaskByProject(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable
    public Task linkTask(@Nullable String userId,
                         @Nullable String oldProjectId,
                         @Nullable String newProjectId,
                         int taskIndex) throws Exception;

    @NotNull
    public List<Task> findByName(@Nullable String userId, @Nullable String taskName) throws Exception;

    @NotNull
    public List<Task> findByDetails(@Nullable String userId, @Nullable String taskDetails) throws Exception;

    @NotNull
    public List<Task> listTaskByProjectSorted(@Nullable String userId,
                                              @Nullable String projectId,
                                              @Nullable String sortType) throws Exception;

    @NotNull
    List<Task> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType) throws Exception;

    @NotNull
    public List<Task> findByUserId(@Nullable String userId) throws Exception;

}

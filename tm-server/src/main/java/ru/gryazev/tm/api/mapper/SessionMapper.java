package ru.gryazev.tm.api.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface SessionMapper {

    @Nullable
    @Select("SELECT * FROM app_session WHERE userId=#{param1} AND id=#{param2}")
    public Session findOneById(@NotNull String userId, @NotNull String id) throws SQLException;

    @Insert("INSERT INTO app_session VALUES " +
            "(#{id}, #{userId}, #{role}, #{signature}, #{timestamp})")
    public void persist(@NotNull Session session) throws SQLException;

    @Delete("DELETE FROM app_session WHERE userId=#{param1} AND id=#{param2}")
    public void remove(@NotNull String userId, @NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_session WHERE userId=#{param1}")
    public void removeAllByUserId(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_session")
    public List<Session> findAll() throws SQLException;

    @Delete("DELETE FROM app_session")
    public void removeAll() throws SQLException;

}

package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Session;
import ru.gryazev.tm.enumerated.RoleType;

public interface ISessionService {

    @Nullable
    public String create(@Nullable String login, @Nullable String pwdHash) throws Exception;

    public void removeAll() throws Exception;

    public void remove(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Contract("null -> fail")
    public void validateSession(@Nullable Session session) throws Exception;

    @Contract("null -> fail")
    public void validateSession(@Nullable Session session, @Nullable RoleType[] roles) throws Exception;

}

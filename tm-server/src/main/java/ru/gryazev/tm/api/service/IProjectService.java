package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    public String getProjectId(@Nullable String userId, int projectIndex) throws Exception;

    @NotNull
    public List<Project> findByUserId(@Nullable String userId) throws Exception;

    @NotNull
    public List<Project> findByName(@Nullable String userId, @Nullable String projectName) throws Exception;

    @NotNull
    public List<Project> findByDetails(@Nullable String userId, @Nullable String projectDetails) throws Exception;

    public void removeAll(@Nullable String userId) throws Exception;

    public List<Project> findByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) throws Exception;

}

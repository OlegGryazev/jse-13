package ru.gryazev.tm.api.service;

import org.apache.ibatis.session.SqlSessionFactory;

public interface ISessionFactoryService {

    public SqlSessionFactory getSqlSessionFactory() throws Exception;

}

package ru.gryazev.tm.api.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Task;

import java.util.List;

public interface TaskMapper {

    @Nullable
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND id=#{param2}")
    public Task findOneById(@NotNull String userId, @NotNull String id);

    @Insert("INSERT INTO app_task VALUES " +
            "(#{id}, #{projectId}, #{userId}, #{name}, #{details}, #{dateStart}, #{dateFinish}, #{status}, #{createMillis})")
    public void persist(@NotNull Task task);

    @Delete("DELETE FROM app_task WHERE userId=#{param1} AND id=#{param2}")
    public void remove(@NotNull String userId, @NotNull String id);

    @Delete("DELETE FROM app_task WHERE userId=#{param1}")
    public void removeAllByUserId(@NotNull String userId);

    @Update("UPDATE app_task SET " +
            "projectId=#{projectId}, " +
            "name=#{name}, " +
            "details=#{details}, " +
            "dateStart=#{dateStart}, " +
            "dateFinish=#{dateFinish}, " +
            "status=#{status} " +
            "WHERE id=#{id}")
    public void merge(@NotNull Task task);

    @NotNull
    @Select("SELECT * FROM app_task ORDER BY createMillis")
    public List<Task> findAll();

    @Delete("DELETE FROM app_task")
    public void removeAll();

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND name LIKE '%' #{param2} '%' ORDER BY createMillis")
    public List<Task> findAllByName(@NotNull String userId, @NotNull String taskName);

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND details LIKE '%' #{param2} '%' ORDER BY createMillis")
    public List<Task> findAllByDetails(@NotNull String userId, @NotNull String taskDetails);

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} ORDER BY createMillis")
    public List<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND projectId=#{param2} ORDER BY ${param3}")
    public List<Task> findAllByProjectIdSorted(@NotNull String userId, @NotNull String projectId, @NotNull String sqlSortType);

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND projectId IS NULL ORDER BY createMillis")
    public List<Task> findAllByUserIdUnlinked(@NotNull String userId);

    @Delete("DELETE FROM app_task WHERE userId=#{param1} AND projectId=#{param2}")
    public void removeByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND projectId=#{param2} ORDER BY createMillis")
    public List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId=#{param1} AND projectId IS NULL ORDER BY #{param2}")
    public List<Task> findAllByUserIdUnlinkedSorted(@NotNull String userId, @NotNull String sqlSortType);

}

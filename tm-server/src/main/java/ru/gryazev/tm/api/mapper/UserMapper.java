package ru.gryazev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.User;

import java.util.List;

public interface UserMapper {

    @Nullable
    @Select("SELECT * FROM app_user WHERE id=#{param1}")
    @Results({
            @Result(property = "roleType", column = "role")
    })
    public User findOneById(@NotNull String id);

    @Insert("INSERT INTO app_user VALUES " +
            "(#{id}, #{login}, #{pwdHash}, #{name}, #{roleType})")
    public void persist(@NotNull User user);

    @Delete("DELETE FROM app_user WHERE id=#{param1}")
    public void remove(@NotNull String id);

    @Delete("DELETE FROM app_user")
    public void removeAll();

    @Update("UPDATE app_user SET " +
            "login=#{login}, " +
            "pwdHash=#{pwdHash}, " +
            "name=#{name}, " +
            "role=#{roleType} " +
            "WHERE id=#{id}")
    public void merge(@NotNull User user);

    @NotNull
    @Select("SELECT * FROM app_user")
    @Results({
            @Result(property = "roleType", column = "role")
    })
    public List<User> findAll();

    @Nullable
    @Select("SELECT * FROM app_user WHERE login=#{param1} AND pwdHash=#{param2}")
    @Results({
            @Result(property = "roleType", column = "role")
    })
    public User findByLoginAndPwd(@NotNull String login, @NotNull String pwd);

}

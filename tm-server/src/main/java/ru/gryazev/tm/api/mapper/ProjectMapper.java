package ru.gryazev.tm.api.mapper;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface ProjectMapper {

    @Nullable
    @Select("SELECT * FROM app_project WHERE userId=#{param1} AND id=#{param2}")
    public Project findOneById(@NotNull String userId, @NotNull String id) throws SQLException;

    @Insert("INSERT INTO app_project VALUES " +
            "(#{id}, #{userId}, #{name}, #{details}, #{dateStart}, #{dateFinish}, #{status}, #{createMillis})")
    public void persist(@NotNull Project project) throws SQLException;

    @Delete("DELETE FROM app_project WHERE userId=#{param1} AND id=#{param2}")
    public void remove(@NotNull String userId, @NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_project WHERE userId=#{param1}")
    public void removeAllByUserId(@NotNull String userId) throws SQLException;

    @Update("UPDATE app_project SET " +
            "name=#{name}, " +
            "details=#{details}, " +
            "dateStart=#{dateStart}, " +
            "dateFinish=#{dateFinish}, " +
            "status=#{status} " +
            "WHERE id=#{id}")
    public void merge(@NotNull Project project) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project ORDER BY createMillis")
    public List<Project> findAll() throws SQLException;

    @Delete("DELETE FROM app_project")
    public void removeAll() throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId=#{param1} AND name LIKE '%' #{param2} '%' ORDER BY createMillis")
    public List<Project> findAllByName(@NotNull String userId, @NotNull String projectName) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId=#{param1} AND details LIKE '%' #{param2} '%' ORDER BY createMillis")
    public List<Project> findAllByDetails(@NotNull String userId, @NotNull String projectDetails) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId=#{param1} ORDER BY createMillis")
    public List<Project> findAllByUserId(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId=#{param1} ORDER BY ${param2}")
    public List<Project> findAllSorted(@NotNull String userId, @NotNull String sqlSortType) throws SQLException;

}

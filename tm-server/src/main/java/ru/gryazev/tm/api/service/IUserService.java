package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.entity.User;
import ru.gryazev.tm.enumerated.RoleType;

import java.sql.SQLException;
import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    public String getUserId(int userIndex) throws Exception;

}
